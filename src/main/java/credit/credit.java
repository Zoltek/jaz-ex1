package credit;

import java.util.ArrayList;
import java.util.List;

public class credit {

	private double kwotaKredytu;
	private int liczbaRat;
	private double oprocentowanie;
	private double oplataStala;
	private String rodzajRaty;
	
	
	public double getKwotaKredytu() {
		return kwotaKredytu;
	}
	public void setKwotaKredytu(double kwotaKredytu) {
		this.kwotaKredytu = kwotaKredytu;
	}
	public int getLiczbaRat() {
		return liczbaRat;
	}
	public void setLiczbaRat(int liczbaRat) {
		this.liczbaRat = liczbaRat;
	}
	public double getOprocentowanie() {
		return oprocentowanie;
	}
	public void setOprocentowanie(double oprocentowanie) {
		this.oprocentowanie = oprocentowanie;
	}
	public double getOplataStala() {
		return oplataStala;
	}
	public void setOplataStala(double oplataStala) {
		this.oplataStala = oplataStala;
	}
	public String getRodzajRaty() {
		return rodzajRaty;
	}
	public void setRodzajRaty(String rodzajRaty) {
		this.rodzajRaty = rodzajRaty;
	}
	
	
	
	
	
	
	
}
