package credit;

public class repayment {

	public int nrRaty;
	public double kwotaNetto;
	public double kwotaBrutto;
	public double prowizja;
	
	
	
	public int getNrRaty() {
		return nrRaty;
	}
	public void setNrRaty(int nrRaty) {
		this.nrRaty = nrRaty;
	}
	public double getKwotaNetto() {
		return kwotaNetto;
	}
	public void setKwotaNetto(double kwotaNetto) {
		this.kwotaNetto = kwotaNetto;
	}
	public double getKwotaBrutto() {
		return kwotaBrutto;
	}
	public void setKwotaBrutto(double kwotaBrutto) {
		this.kwotaBrutto = kwotaBrutto;
	}
	public double getProwizja() {
		return prowizja;
	}
	public void setProwizja(double prowizja) {
		this.prowizja = prowizja;
	}
	
	
	
}
